<?php
/**
 * @file
 * Commons Hashtags module.
 */

/**
 * Implements hook_filter_default_formats_alter().
 *
 * Adds the Hashtag filter to the input filters.
 */
function commons_hashtags_filter_default_formats_alter(&$formats) {
  if (!empty($formats['filtered_html'])) {
    $formats['filtered_html']['filters']['filter_hashtags'] = array(
      'status' => '1',
      'weight' => '5',
    );
  }
  if (!empty($formats['full_html'])) {
    $formats['full_html']['filters']['filter_hashtags'] = array(
      'status' => '1',
      'weight' => '5',
    );
  }
}

/**
 * Implements hook_filter_info().
 */
function commons_hashtags_filter_info() {
  $filters['filter_hashtags'] = array(
    'title' => t('Convert Hashtags into links'),
    'description' => t('Turn #words into links which link to taxonomy terms'),
    'process callback' => '_commons_hashtags_filter_process',
    'cache' => TRUE,
  );
  return $filters;
}

/**
 * Filter process callback to replace hastags with taxonomy links.
 */
function _commons_hashtags_filter_process($text, $filter, $format) {
  $hashtags = _commons_hashtags_extract_hashtags($text);

  // No hashtags found? Return directly.
  if (empty($hashtags)) {
    return $text;
  }

  // Load the term objects or create terms if they don't exist yet.
  $terms = _commons_hashtags_load_terms($hashtags);

  // Do the actual replacement.
  foreach ($terms as $hashtag => $term) {
    $link = l($hashtag, 'taxonomy/term/' . $term->tid, array(
      'attributes' => array('rel' => 'term'),
    ));
    $text = preg_replace('/(' . $hashtag . ')\b/', $link, $text);
  }

  return $text;
}

/**
 * Implements hook_node_presave().
 *
 * Detect hashtags and store connect these terms to the node.
 */
function commons_hashtags_node_presave($node) {
  $body_fields = field_get_items('node', $node, 'body');

  // Body empty? Return.
  if (empty($body_fields)) {
    return;
  }
  $body = $body_fields[0];

  // If we have hashtags in the body, map this node to the terms.
  if ($hashtags = _commons_hashtags_extract_hashtags($body['value'])) {
    // Get the existing topics. If they are empty, create an empty array.
    $existing_terms = &$node->field_topics[LANGUAGE_NONE];
    if (empty($existing_terms)) {
      $existing_terms = array();
    }

    // Load the term objects or create terms if they don't exist yet.
    $terms = _commons_hashtags_load_terms($hashtags);

    $existing_tids = array();
    foreach ($existing_terms as $existing_term) {
      $existing_tids[] = $existing_term['tid'];
    }

    foreach ($terms as $hashtag => $term) {
      // If the hastags is not related to the node as topic, add it.
      if (!in_array($term->tid, $existing_tids)) {
        $existing_terms[] = (array) $term;
      }
    }
  }
}

/**
 * Load the term objects or create terms if they don't exist yet.
 */
function _commons_hashtags_load_terms($hashtags) {
  $terms = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load('topics');
  foreach ($hashtags as $hashtag) {
    $name = str_replace('#', '', $hashtag);
    // Load the term by name.
    $term = taxonomy_get_term_by_name($name, $vocabulary->machine_name);
    if (empty($term)) {
      // Term does not exist yet, so create it.
      $term = (object) array(
        'name' => $name,
        'vid' => $vocabulary->vid,
      );
      taxonomy_term_save($term);
    }
    $terms[$hashtag] = is_array($term) ? current($term) : $term;
  }
  return $terms;
}

/**
 * Helper function to extract all hashtags in the text.
 */
function _commons_hashtags_extract_hashtags($text) {
  // 1) 2+ character after #
  // 2) Don't start with or use only numbers (0-9) (#123abc, #123 etc).
  // 3) Letters - digits work correct (#abc123, #conference2013).
  // 4) No Special Characters “!, $, %, ^, &, *, +, .”
  // 5) No Spaces.
  // 6) May use an underscore. Hyphens and dashes will not work.
  // 7) <p>#hashtag</p> - is valid.
  // 8) <a href="#hashtag">Link</p> - is not valid.
  $pattern = "/[\s>]+?(#[[:alpha:]][[:alnum:]_]*[^<\s[:punct:]])/iu";

  $tags_list = array();
  preg_match_all($pattern, $text, $tags_list);

  // No hashtags has been found.
  if (empty($tags_list[0])) {
    return FALSE;
  }

  return $tags_list[1];
}
